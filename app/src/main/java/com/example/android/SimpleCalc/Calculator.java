/*
 * Copyright 2018, Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.SimpleCalc;

/**
 * Utility class for SimpleCalc to perform the actual calculations.
 *
 * To add a new operation:
 *  1) add an entry to the Operator enumeration in Calculator.java
 *  2) add an operation method to the Calculator.java that actually performs the operation
 *  3) add a corresponding case to the switch() statement in MainActivity.java
 *  4) add a button to layout, onClick handler to XML, and edit handler method in MainActivity.java
 */
public class Calculator {

    // Available operations that are used by switch() statement in MainActivity class
    public enum Operator {ADD, SUB, DIV, MUL, PWR, SQRT}

    /**
     * Addition operation
     */
    public double add(double firstOperand, double secondOperand) {
        return firstOperand + secondOperand;
    }

    /**
     * Subtract operation
     */
    public double sub(double firstOperand, double secondOperand) {
        return firstOperand - secondOperand;
    }

    /**
     * Divide operation
     */
    public double div(double firstOperand, double secondOperand) {
        return firstOperand / secondOperand;
    }

    /**
     * Multiply operation
     */
    public double mul(double firstOperand, double secondOperand) {
        return firstOperand * secondOperand;
    }

    /**
     * Power operation
     * TODO: Add code to pwr() that raises firstOperand to power of secondOperand
     */
    public double pwr(double firstOperand, double secondOperand) {
        double returnValue = 0;
        // If firstOperand is not negative and secondOperand is not negative, then
        //    use static method from Math class to set returnValue to firstOperand raised to power of secondOperand
    /*    if ((firstOperand >= 0) && (secondOperand >= 0)) {
            // remove the following for starter
            returnValue = Math.pow(firstOperand, secondOperand);
        }
     */
        return returnValue;
    }

    /**
     * Square root operation
     * TODO: Add code to sqrt() that finds the square room of the firstOperand
     */
    public double sqrt(double firstOperand) {
        double returnValue = 0;
        // If firstOperand is not negative, then
        //    use static method from Math class to set returnValue to square root of firstOperand
    /*    if (firstOperand >= 0) {
            // remove the following for starter
            returnValue = Math.sqrt(firstOperand);
        }
     */
        return returnValue; //remove this for lab
    }
}