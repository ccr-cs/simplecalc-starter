Download, extract, rename, move
-------------------------------
Use the menu at upper-right (the three dots) to Download this repository, which will download a zip archive of the entire project.
Extract the zip file, rename the directory to SimpleCalc, and copy the SimpleCalc directory to your AndroidStudioProjects directory.

Open the project in Android Studio
----------------------------------
Open Android Studio, Select **File**, then **Open**, then select the **SimpleCalc** project.

Once the project is open, you will probably see some errors or warnings. 

+ Sync Android SDKs is an error that is caused because the original project was created on a machine with specific pathnames. Android Studio should fix this by pointing the SDK to the correct directory. Just click OK, and Android Studio should fix it.

+ If your version of Android Studio is old, you might see errors about updating your version of the IntelliJ IDEA or Android Studio or similar. It’s important to keep your version of Android Studio updated, so just install updates and the app should work.

+ It’s possible that you will see errors or warnings indicating that the project’s version of Android Studio, IntelliJ, Gradle, or some other component is out of date. These errors or warnings are impossible to predict, but Android Studio usually provides good descriptions of the issues, and can point you in the right direction for addressing them.
